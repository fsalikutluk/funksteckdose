package com.example.steckdosen.repository

import com.example.steckdosen.model.Steckdose
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

/**
 * Das Interface das wir brauchen um auf die Datenbank zuzugreifen.
 * Durch das : JpaRepository haben wir mehr Funktionen zur Verfügung als wir auf den ersten Blick sehen.
 */
interface SteckdoseRepository : JpaRepository<Steckdose, Long> {
    fun findByCode(code: String) : Optional<Steckdose>

}