package com.example.steckdosen.controller

import com.example.steckdosen.model.LetzterZugriff
import com.example.steckdosen.model.Steckdose
import com.example.steckdosen.service.SteckdosenService
import org.springframework.http.HttpRequest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Das Objekt das die Verbindung zu unserer HTML Seite index.html ermöglicht
 */
@RestController
class SteckdosenController(final var steckdosenService: SteckdosenService) {


    /**
     * Die Methode die wir aufrufen müssen um die Steckdose ein zu schalten
     */
    @RequestMapping(value= ["/an"], method=[RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun an(@RequestBody steckdose: Steckdose): ResponseEntity<HttpStatus> {
        steckdosenService.an(steckdose)
        return ResponseEntity.ok(HttpStatus.OK);
    }

    /**
     * Die Methode die wir aufrufen müssen um die Steckdose aus zu schalten
     */
    @RequestMapping(value= ["/aus"], method=[RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody

    fun aus(@RequestBody steckdose: Steckdose): ResponseEntity<HttpStatus> {
        steckdosenService.aus(steckdose)
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/letzter-zugriff")
    fun letzerZugriff(): ResponseEntity<LetzterZugriff> {
        val letzterZugriff = steckdosenService.lastAccess()
        return ResponseEntity.ok(letzterZugriff);
    }

}