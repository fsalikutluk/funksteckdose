package com.example.steckdosen

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SteckdosenApplication

fun main(args: Array<String>) {
	runApplication<SteckdosenApplication>(*args)
}
