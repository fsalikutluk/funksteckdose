package com.example.steckdosen.service

import com.example.steckdosen.model.LetzterZugriff
import com.example.steckdosen.model.Steckdose
import com.example.steckdosen.repository.SteckdoseRepository
import com.example.steckdosen.util.CommandLineExecuter
import com.pi4j.io.gpio.GpioController
import com.pi4j.io.gpio.GpioFactory
//import com.salikutluk.smarthome.commandline.CommandLineExecuter
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.util.*

@Service
class SteckdosenService(val repository: SteckdoseRepository, val executer: CommandLineExecuter = CommandLineExecuter()) {

    @Value("\${send.script.path}")
    var pathToScript: String? = null

    fun an(steckdose: Steckdose) {
        sendCommand(steckdose, true)
    }

    fun aus(steckdose: Steckdose) {
        sendCommand(steckdose, false)
    }

    private fun sendCommand(steckdose: Steckdose, isOn: Boolean) {
        executer.executeCommand("python3", pathToScript, "-p", steckdose.pulslength, "-t", steckdose.protocol, steckdose.code)
       // executer.executeCommand("touch", steckdose.pulslength+"-"+steckdose.protocol+"-"+steckdose.code)
        val foundSteckdose: Optional<Steckdose> = repository.findByCode(steckdose.code)
        if (foundSteckdose.isPresent) {
            val steckDose = foundSteckdose.get()
            steckDose.isOn = isOn
            steckDose.lastChange = Date()
            repository.save(steckDose)
        } else {
            steckdose.isOn = isOn
            steckdose.lastChange = Date()
            repository.save(steckdose)
        }
    }

    public fun lastAccess():LetzterZugriff{
        val letzter = repository.findAll();
        if(!letzter.isEmpty()){
            return LetzterZugriff(letzter.get(0).lastChange)
        }else{
            return LetzterZugriff(null)
        }

    }

}
