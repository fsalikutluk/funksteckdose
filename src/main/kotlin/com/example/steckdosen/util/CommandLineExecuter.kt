package com.example.steckdosen.util

import org.springframework.stereotype.Component
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*


@Component
class CommandLineExecuter {
    fun executeCommand(vararg commands: String?): String {
        val output = StringBuffer()
        try {
            log(*commands)
            val p = Runtime.getRuntime().exec(commands)
            p.waitFor()
            val reader = BufferedReader(InputStreamReader(p.inputStream))
            var line: String? = ""
            while (reader.readLine().also { line = it } != null) {
                output.append(line)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        log(output.toString())
        return output.toString()
    }

    @Synchronized
    private fun log(vararg messages: String?) {
        var command = ""
        for (message in messages) {
            command += " $message"
        }
        println(format.format(Date()) + ": " + command)
    }

    companion object {
        private val format = SimpleDateFormat("MM/dd/yyyy hh:mm:ss:SSS")
    }
}