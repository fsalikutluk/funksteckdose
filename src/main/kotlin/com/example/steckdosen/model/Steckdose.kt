package com.example.steckdosen.model

import com.fasterxml.jackson.annotation.JsonInclude
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Transient

/**
 * Das Objekt das wir in der Datenbank speichern.
 */
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Steckdose(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long, @Transient val protocol: String, @Transient val pulslength: String, val code: String, var isOn: Boolean?, var lastChange: Date?) {
    override fun equals(other: Any?): Boolean {
        if(other !is Steckdose)
            return false
        return code.equals((other as Steckdose).code)
    }
}
