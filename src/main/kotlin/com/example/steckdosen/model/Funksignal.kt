package com.example.steckdosen.model

class Funksignal(val wert: Long?, val signaldauerMikrosekunden: Long, val kanal: Integer) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Funksignal) return false

        if (wert != other.wert) return false
        if (kanal != other.kanal) return false

        return true
    }

    override fun hashCode(): Int {
        var result = wert?.hashCode() ?: 0
        result = 31 * result + kanal.hashCode()
        return result
    }
}