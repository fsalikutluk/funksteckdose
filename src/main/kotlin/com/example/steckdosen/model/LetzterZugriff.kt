package com.example.steckdosen.model

import java.util.*

data class LetzterZugriff(var timestamp: Date?)